# README #

Script for analysis C headers (*.h) according to the standard ISO C99, which creates database of functions founded in headers.

### About project ###

* run with Python 3
* school project for IPP - VUT FIT
* DO NOT COPY - ONLY FOR INSPIRATION

### Input parameters ###

* --help
* --input=fileordir input file or dir for analysis (UTF-8)
* --output=filename output XML file
* --pretty-xml=k formats outputfile - makes a pretty file
* --no-inline skips functions declared with the specifier inline
* --max-par=n only functions with n or less parameters
* --no-duplicates more functions with same names -> only first save
* --remove-whitespace only one whitespace everywhere

### Usage ###

* *python3 cha.py --input=header.h --output=output.xml*
* *python3 cha.py --input=header.h --output=output.xml --pretty-xml=5 --max-par=2*
* *python3 cha.py --input=dir/ --output=functions.xml --no-duplicates --remove-whitespace*

### Author ###
* Tomas Hynek, xhynek09
* https://bitbucket.org/strajky/