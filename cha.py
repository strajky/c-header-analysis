#!/usr/bin/python3
# -*- coding: utf-8 -*-
#CHA:xhynek09
#Projekt 2 - CHA: C Header Analysis v Python 3 pro IPP 2015/2016
#Jmeno a prijmeni: Tomas Hynek (c) 2016; Login: xhynek09
#Datum: 23.3.2016, Brno

import os
import re
import sys
import getopt
import numbers

Files = []        # seznam vsech zdrojovych souboru ze vsech adresaru
functionList = [] # seznam vkladanych funkci do xml
inputFile = ""    # vstupni zdrojovy soubour nebo adresar
outputFile = ""   # vystupni soubor
textFile = ""     # string vstupniho souboru
max_par = 0       # hodnota parametru max-par
subdirFlag = 0    # indikace prohledavani pouze v adresarich, bez podadesaru
funSpaces = ""     # odsazeni o 4 mezery pro funkce
paramSpaces = ""   # odsazeni o 8 mezer pro parametry
no_inline = False          # flag pro parametr no-inline
absolutPath = False        # indikace, zda vypsat celou absolutni cestu (False) nebo pouze nazev souboru (True)
no_duplicates = False      # flag pro parametr no-duplicates
removewhitespace = False   # flag pro parametr remove-whitespace
inputFlag = 0; outputFlag = 0; prettyFlag = 0; parFlag = 0; inlineFlag = 0; duplicatesFlag = 0; removeFlag = 0; # flagy pro kontrolu duplicit
xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"    # pomocny string pro XML

###################################################################################
############################### HLAVNI PROGRAM ####################################
# Hlavni program odkud s volaji vsechny funkce pro dosahnuti spravenho xml vystupu
def main():

   global inputFile, outputFile, subdirFlag, Files, xmlString, prettyFlag
   dirpath = ''
   score = 0
   totalScore = 0

   # Volani funkce pro zpracovani vstupnich parametru 
   paramParser()

   # Vyhledat a ulozit vsechny vstupni soubory v adresari a podadresari
   if inputFile == '':              # pokud nebyl zadan vstupni soubor
      scanFileDir("./")             # hledej soubory ve slozce a v podadresarich
      if ((re.search("/$",dirpath)) == None):
         dirpath += '/'

      dirpath = './'                # uloz cestu slozky
   elif os.path.isdir(inputFile):   # pokud je na vstupu slozka
      scanFileDir(inputFile)        # hledej soubory ve slozce a v podadresarich
      dirpath = inputFile           # uloz cestu slozky
      if ((re.search("/$",dirpath)) == None):
         dirpath += '/'
   elif os.path.isfile(inputFile):  # pokud je na vstupu soubor
      Files.append(inputFile)       # pridej ho do seznamu
      dirpath = ''                  # cesta slozky bude prazda
   else:
      sys.stderr.write("Error Dir: chybny format vstupniho souboru\n")  
      sys.exit(2)

   # Nacteni vystupniho souboru pro zapis 
   if outputFile == '':
      outputFile = sys.stdout
   else:
      try:
         outputFile = open(outputFile, 'w', encoding='utf-8')     
      except:
         sys.stderr.write("Error OutputFile: nelze nacist vystupni soubor\n")
         sys.exit(3)

   # odradkovani pri parametru pretty-xml
   if (prettyFlag > 0):
      xmlString += '\n'

   # pridani xml tagu funkce a cesty
   xmlString += "<functions dir=\""+dirpath+"\">"

   # HLAVNI ZPRACOVANI - analyza vsech zdrojovych souboru a jejich nasledne parsrovani a vypisovnaji do xml formatu
   for file in Files:
      functionsArray = fileParser(file)
      functionParser(functionsArray, file, dirpath)

   if (prettyFlag > 0):
      xmlString += '\n'
   xmlString += "</functions>\n"

   # VYPIS DO SOUBORU NEBO NA STDOUT
   outputFile.write(xmlString)

   sys.exit(0)

###################################################################################
####################### ZPRACOVANI VSTUPNICH PARAMETRU ############################
# Zpracovani a kontrola vstupnich parametru programu
def paramParser():

   global inputFile, outputFile, inputFlag, outputFlag, parFlag, prettyFlag, removewhitespace, no_inline, max_par, no_duplicates, funSpaces, paramSpaces, inlineFlag, duplicatesFlag, removeFlag
   count = 1            # celkovy pocet zpracovanych parametru
   paramError = False   # indikace vypsani erroru
   
   # ZPRACOVANI VSTUPNICH PARAMETRU
   for pos in range(len(sys.argv)):
      # HELP
      if sys.argv[pos].find('--help') != -1:
         if len(sys.argv[1:]) > 1:  # --help se nesmi kombinovat s jinymi parametry
            sys.stderr.write("Error Param: chybne zadane vstupni parametry\n")
            sys.exit(1)
         else:
            help()      # vypis napovedy
            sys.exit(0)
      # INPUT
      if sys.argv[pos].find('--input') != -1:
         count += 1
         inputFlag += 1
         inputFile = (sys.argv[pos])[8:]         # ulozeni input filename
      # OUTPUT
      if sys.argv[pos].find('--output') != -1:
         count += 1
         outputFlag += 1
         outputFile = (sys.argv[pos])[9:]        # ulozeni output filename
      # PRETTY-XML
      if sys.argv[pos].find('--pretty-xml') != -1:
         count += 1
         prettyFlag += 1
         pretty_xml = (sys.argv[pos])[13:]      # ulozeni hodnoty
      # NO-INLINE
      if sys.argv[pos].find('--no-inline') != -1:
         count += 1
         inlineFlag += 1
         no_inline = True                       # nastaveni, ze byl parametr zadan
      # MAX-PAR
      if sys.argv[pos].find('--max-par') != -1:
         count += 1
         parFlag += 1
         max_par = (sys.argv[pos])[10:]         # ulozeni hodnoty
      # NO-DUPLICATES
      if sys.argv[pos].find('--no-duplicates') != -1:
         count += 1
         duplicatesFlag += 1
         no_duplicates = True                   # nastaveni, ze byl parametr 
      # REMOVE-WHITESPACES
      if sys.argv[pos].find('--remove-whitespace') != -1:
         removewhitespace = True;               # nastaveni, ze byl parametr 
         removeFlag += 1
         count += 1

   # kontrola poctu zpracovanych parametru a detekovani chyby zpracovani
   if (len(sys.argv) != count):
      sys.stderr.write("Error Param: chybne zadane vstupni parametry\n")
      sys.exit(1)
   # kontrola duplicity parametru
   if (inputFlag > 1 or outputFlag > 1 or parFlag > 1 or prettyFlag > 1 or inlineFlag > 1 or duplicatesFlag > 1 or removeFlag > 1):
      sys.stderr.write("Error Param: zadany duplicitni vstupni parametry\n")
      sys.exit(1)
   # kontrola zda nebylo zadan prazdny vstupni soubor nebo prazdny vystupni soubor pri prepinacich --input a --output
   if ((outputFlag == 1 and outputFile == "") or (inputFlag == 1 and inputFile == "")):
      sys.stderr.write("Error Param: chybne zadane vstupni parametry\n")
      sys.exit(1)
   
   # kontrola zda je parametr max-par integer
   if (parFlag > 0):
      try:
         max_par = int(max_par)
         if (max_par < 0):
            sys.stderr.write("Error Param: chybne zadane vstupni parametry\n")
            sys.exit(1)
      except ValueError:
         sys.stderr.write("Error Param: chybne zadane vstupni parametry\n")
         sys.exit(1)

   # typova kontrola parametru pretty-xml
   if (prettyFlag > 0):   
      if (pretty_xml != ''):
         try:
            funSpaces = '\n'+ str(int(pretty_xml) * ' ')
            paramSpaces = '\n'+ str((2 * int(pretty_xml)) * ' ')
            if (int(pretty_xml) < 0):
               sys.stderr.write("Error Param: chybne zadane vstupni parametry\n")
               sys.exit(1)
         except ValueError:
            sys.stderr.write("Error Param: chybne zadane vstupni parametry\n")
            sys.exit(1)
      else:
         funSpaces = '\n'+ 4 * ' '     # odsazeni o 4 mezery pro funkce
         paramSpaces = '\n'+ 8 * ' '   # odsazeni o 8 mezer pro parametry

###################################################################################
######################### SMAZANI NEPOTREBNEHO KODU ###############################
# Prvnim krokem je vzdy potreba vstupni soubor upravit, coz obnasi odstraneni komentaru,
# retezcu, maker, strukur, obsahu "{...}" funkci pri definici, prebytecnych radku a 
# popripade odstraneni bilych znaku
# Na konci funkce zybodu jen samostatne definice funkci oddelene strednikem, dle ktereho
# se rozdeli do pole
def fileParser(file):
   global textFile, removewhitespace

   # OTEVRENI VSTUPNIHO SOUBORU
   try:
      openFile = open(file, 'r', encoding='utf-8')      
      textFile = openFile.read()
      openFile.close()
   except:
      sys.stderr.write("Error InputFile: nelze otevrit vstupni soubor\n")
      sys.exit(2)

   # ODSTRANENI KOMENTARU A BLOKOVYCH KOMENTARU
   textFile = re.sub(r"\s*\/\/.*", r"", textFile)                 # //
   textFile = re.sub(r"\/\*(\*(?!\/)|[^*])*\*\/", r"", textFile)  # /**/

   # ODSTRANENI RETEZCU
   textFile = re.sub(r"\"(.)*\"", "", textFile)
   textFile = re.sub(r"\'(.)*\'", "", textFile)
   textFile = re.sub(r"\s*\"\s*", "", textFile)
   textFile = re.sub(r"\s*\'\s*", "", textFile)
   
   # ODSTRANENI JEDNORADKOVYCH I VICERADKOVYCH MAKER
   textFile = re.sub(r"[ \t]*#define(?:.*\\\n)+.*", r"", textFile)
   textFile = re.sub(r"#[^\n]*", r"", textFile)

   # ODSTRANENI TYPEDEF KONSTRUKCI a STRUKTUR
   textFile = re.sub(r"typedef.*\s*{[^}]*}.*;", r"", textFile)
   textFile = re.sub(r"struct.*\n{[^}]*};", r"", textFile)      
   textFile = re.sub(r"struct.*{[^}]*};", r"", textFile)
   textFile = re.sub(r"enum.*\n{[^}]*};", r"", textFile)
   textFile = re.sub(r"typedef\s+enum\s*{[^}]*}[^;]+;", r";", textFile)
   #textFile = re.sub(r"(?:typedef|enum|struct)*\s*(?:\w|\s|\{|;|\*)*\s*}\s*\w*;", r";", textFile)
   textFile = re.sub(r"\s*(?:typedef|enum|struct)(?:\w|\s)*;", r"", textFile)
   textFile = re.sub(r"typedef(?:.)*;", r"", textFile)

   # ODSTRANENI KONSTRUKCE ZANORENYCH FUNKCI - ZAVOREK {...}
   bracesParser()

   # ODSTRANENI PREBYTECNYCH RADKU
   textFile = re.sub(r"^\n+", r"", textFile)
   textFile = re.sub(r"\n+$", r"", textFile)
   textFile = re.sub(r"^\s+", r"", textFile)
   textFile = re.sub(r"\s+$", r"", textFile)

   # ODSTRANENI WHITE SPACE PRI PARAMETRU --white-space
   if (removewhitespace == True):
      textFile = re.sub(r"\s+", r" ", textFile)
      textFile = re.sub("(\*) (\*)", r'**', textFile) # mezera mezi vezdickami - **
      textFile = re.sub("(.) (\*)", r'\1*', textFile) # mezera mezi typem a hvezdickou - int*
      textFile = re.sub("(\*) (.)", r'*\2', textFile) # mezera mezi hvezdickou a promennou - *value  

   # ROZDELENI FUNKCI DLE STREDNIKU DO POLE
   functionsArray = textFile.split(';')

   return functionsArray

###################################################################################
########################## ZPRACOVANI ZAVOREK FUNKCI ##############################
# Funkce hleda prvni vyskyt slozene zavorky "{" a poznaci si pozici
# Dale hleda posledni vyskyt slozene zavorky "}" a poznaci si pozici
# Pote odstrani vse co je obsazeno v techto slozenych zavorkach a nahradi to strednikem
def bracesParser():
   global textFile
   findBraces = True
   counterBraces = 0

   startPosition = textFile.find('{', 0)

   # Zpracovavej dokud jsme nenarazili na posledni uzaviraci slozenou zavorku "}" 
   # (respektive nezpracovaly vsechny slozene zavorky)
   while findBraces == True:
      startPosition = textFile.find('{', 0)     # ulozeni pozice prvni slozene zavorky
      
      # Pokud byla nalezena nejaka slozena zavorka, nastav start pozice pro oseknuti
      # Pokud nebyla nalezana "{", tak konci cyklus
      if (startPosition != -1):
         findBraces = True
         position = startPosition
      else:
         findBraces = False
         break

      # Funkce pocita jednotlive slozene zavorky a popripade jejich zanoreni pr.: "{{{...}}}"
      # Jakmile se vsechny pary zavorek najdou, cyklus konci a ulozi se posledni pozice, kde se nachazime
      while True:
         if (textFile[position] == '{'):
            counterBraces += 1
         elif (textFile[position] == '}'):
            counterBraces -= 1
         elif (counterBraces == 0):
            break

         position += 1

         if (counterBraces == 0):
            break

      endPosition = position     # ulozeni posledni pozice pro orezani

      # Pro oseknuti retezce se pouzije naleznuta prvni pozice dane slozene zavorky ("{") a posledni pozice slozene zavorky ("}")
      textFile = textFile[:startPosition]+';'+textFile[endPosition:]

###################################################################################
############################## ANALYZA SOUBORU ####################################
# Vstupem tohoto parseru je pole s definicemi funkci (navrat, typ, jmeno, parametry), zadana
# cesta a cesta ke zpracovavanemu souboru
# Funkce provadi oddeleni: nazev funkce, navratoveho typu a parametru, ktere se ulozi do pole
def functionParser(functionsArray, filepath, dirpath):
   global no_inline, parFlag, functionList, no_duplicates

   functionList = []    # inicializace pole pro jiz zpracovane funkce
   size = len(dirpath)
   filepath = filepath[size:]

   # Prochazeni pole po jednotlivych funkci ze souboru
   for function in functionsArray:
      varArgs = 'no'

      if function != '':
         # JMENO FUNKCE A NAVRATOVY TYP FUNKCE
         firstPart = re.search('^[^(]*', function).group()
         position = len(firstPart)
         
         # odstraneni prebytecnych bilych znaku na zacatku a konci
         firstPart = re.sub('^\s*', '', firstPart)    
         firstPart = re.sub('\s*$', '', firstPart)

         # ziskani jmena a navratoveho typu pomoci regularniho vyrazu
         functionNameSearch = re.search('\w+$', firstPart)
         functionName = functionNameSearch.group()
         functionRetType = firstPart[:functionNameSearch.span()[0]]

         # odstraneni prebytecnych bilych znaku na zacatku a konci
         functionRetType = re.sub('^\s*', '', functionRetType)
         functionRetType = re.sub('\s*$', '', functionRetType)

         # NO-INLINE PARAMETR - preskocit funkce obsahujici inline
         if ((no_inline == True) and (firstPart.find('inline') != -1)):
            continue

         # OSETRENI DLE ZADANI S PARAMETREM no-duplicates
         if ((no_duplicates == True) and (functionList.count(functionName) > 0)):
            continue
         
         # PRIDANI NAZVU FUNKCE DO SEZNAMU JIZ ZPRACOVANYCH
         functionList.append(functionName)
         
         # ZISKANI PARAMETRU FUNKCE
         secondPart = function[position:]
         secondPart = re.sub('\(', '', secondPart)
         secondPart = re.sub('\)', '', secondPart)
         functionParameters = secondPart.split(',')   # cely retezec s parametry se dle "," rozdeli do pole

         countParam = 0

         # Prohledani pole s parametry a zjisteni zda funkce ma promenny pocet parametru a pocet parametru
         for parameter in functionParameters:
            parameter = re.sub('^\n*', '', parameter)    # odstraneni prebytecnych radku na konci a zacatku
            parameter = re.sub('\n*$', '', parameter)

            # 1. pokud ma funkce promenny pocet parametru (...) -> varargs bude "yes"
            # 2. pokude neni pole prazdne nebo neobsahuje pouze "void", incrementuj pocet parametru funkce
            if ((re.search("\.\.\.",parameter)) != None):   # 1.
               varArgs = 'yes'
            elif ((parameter != '') and ((re.search("\s*void\s*$",parameter)) == None)):  # 2.
               countParam += 1

         # OSETRENI DLE ZADANI S PARAMETREM max-par=n, kde n musi byt <= poctu parametru funkce
         if ((parFlag != 0) and (countParam > max_par)):
            continue

         # VYPSANI VSECH ZISKANYCH INFORMACI O FUNKCI DO XML
         printFunction(dirpath, filepath, functionName, varArgs, functionRetType, functionParameters)

###################################################################################
############################### PARSER FUNKCE #####################################
# Funkce dostane jiz rozsekane data pro jednu jedninou funkci, o ktere je jiz znamo jmeno,
# navrat. typ, jednotlive parametry a jejich vlastnosti
# Pote pridava jendnotlive funkce a parametry do retezce
def printFunction(dirpath, filepath, name, varargs, rettype, parameters):
   global xmlString, funSpaces, paramSpaces, prettyFlag, removewhitespace

   # ZACATEK FUNKCE - nazev funkce, navrat. typ, cesta k souboru, promenlivy pocet parametru (yes, no)
   xmlString += funSpaces+"<function file=\""+filepath+"\" name=\""+name+"\" varargs=\""+varargs+"\" rettype=\""+rettype+"\">"

   number = 1

   # VYPSANI JEDNOTLIVYCH PARAMETRU DANE FUNKCE
   for parameter in parameters:
      parameter = re.sub('^\s*', '', parameter)
      parameter = re.sub('\s*$', '', parameter)

      # Pokud neni pole prazdne nebo parametr nejsou "..." nebo neni zde pouze samostatny retezec "void", tak vypis do stringu
      if ((parameter != "") and ((re.search("\.\.\.",parameter)) == None) and ((re.search("\s*void\s*$",parameter)) == None)):

         # odstraneni nazvu promenne parametru
         paramEnd = re.search('(?:\[(?:.)*?\])*$', parameter)
         #paramSearch = re.search('\w+\s*(?:\[(?:\d|)*?\])*?$', parameter)
         paramSearch = re.search('\w+\s*(?:\[(?:\d|\w)*?\])*?(?:\*)?$', parameter)
         param = parameter[:paramSearch.span()[0]]

         # pri remove-whitespace odstran mezeru
         if ((re.search('\[(?:\d|\w)*?\]', paramEnd.group()) != None) and (removewhitespace == True)):
            param = re.sub('\s*$', '', param)
         
         # pokud obsahuje promennna "[]" dopln to dodatecne
         parameter = param + paramEnd.group()

         parameter = re.sub('\s*$', '', parameter) # odsraneni na zacatku a konci prebytecnych bilych znaku
         parameter = re.sub('^\s*', '', parameter)

         # VLOZENI PARAMETRU
         xmlString += paramSpaces+"<param number=\""+str(number)+"\" type=\""+parameter+"\" />"
         number += 1
   
   # VLOZENI KONEC FUNKCE
   xmlString += funSpaces+"</function>"

###################################################################################
########################## SCAN SOUBORU, SLOZEK, PODSLOZEK ########################
# Funkce pro skenovani souboru ve slozkach a jejih podslozkach
def scanFileDir(dir):   # sken do hloubky i s podadresari
   for dirpath, dirnames, filenames in os.walk(dir):
      for file in [f for f in filenames if f.endswith(".h") or f.endswith(".H")]:
         Files.append(os.path.join(dirpath, file))    # ukladani jednotlivych souboru do pole s cestou
def scanFiles(dir):     # sken pouze aktualniho adresare bez podadresaru
   for file in os.listdir(dir):
      if file.endswith(".h") or file.endswith(".H"):
         path = os.path.join(dir, file)
         Files.append(os.path.join(path))    # ukladani jednotlivych souboru do pole s cestou

###################################################################################
############################# HELP - NAPOVEDA #####################################
def help():
   print (" ################################################################\n",\
   "####### Projekt 2 -  CHA: C Header Analysis v Python ###########\n",\
   "############### Autor: Tomas Hynek (c) 2016 ####################\n",\
   "############### Login: xhynek09             ####################\n",\
   "###############     Napoveda ke skriptu     ####################\n",\
   "## --help \t\tvypise na standardni vystup napovedu skriptu\n",\
   "## --input=fileordir\tzadany vstupni soubor nebo adresar se zdrojovym kodem v jazyce C\n",\
   "## --output=filename\tzadany textovy vystupni soubor v kodovanim UTF-8\n",\
   "## --pretty-xml=k\tskript zformátuje výsledný XML dokument dle pravidel v zadani\n",\
   "## --no-inline\t\tskript přeskočí funkce deklarované se specifikátorem inline\n",\
   "## --max-par=n\t\tskript bude brát v úvahu pouze funkce, které mají n či méně parametrů\n",\
   "## --no-duplicates\tpokud se v souboru vyskytne více funkcí se stejným jménem,\n\t\t\ttak se do výsledného XML souboru uloží pouze první z nich\n",\
   "## --remove-whitespace\tv obsahu atributu rettype a type nahradi vsechny vyskyty jinych bilych\n\t\t\tznaku, nez je mezera, mezerou a odstrani z nich vsechny prebytecne mezery\n")

# spusteni hlavniho programu
if __name__ == '__main__':
   main()